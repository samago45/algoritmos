package org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TotalResultados {
    private double totalTiempoEspera;
    private double totalTiempoRetorno;
    private double totalTiempoRespuesta;
}
