package org.algoritmo.plaficacion.algoritmoplanificacionprocesos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.Tabla.TableViewInitializer;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.algoritmos.FCFS;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.diagrama.Gantt;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models.PCB;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models.ResultadoProcesamiento;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models.TotalPromedio;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models.TotalResultados;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.algoritmo.plaficacion.algoritmoplanificacionprocesos.Tabla.TableViewInitializer.CalculoTableView;
import static org.algoritmo.plaficacion.algoritmoplanificacionprocesos.Tabla.TableViewInitializer.TotalTableView;


public class Home {


    ExcelController excelController = new ExcelController();

    @FXML
    private Button cargarExcelButton;

    @FXML
    private Label nombreArchivoLabel;
    @FXML
    private TableView<PCB> tableView;


    @FXML
    private TableView<ResultadoProcesamiento> tableViewProcesamientos;
    @FXML
    private TableView<TotalResultados> tableViewProcesamientosTotal;

    private ObservableList<PCB> listaPCB = FXCollections.observableArrayList();



   /* public void initialize() {
        TableViewInitializer.initializeTableView(tableView, listaPCB);

    }*/

  /*  @FXML
    private void AlgoritmoFCFS() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Seleccionar archivo CSV");
        // Filtro para mostrar solo archivos con extensión .csv
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Archivos CSV (*.csv)", "*.csv");
        fileChooser.getExtensionFilters().add(extFilter);
        // Mostrar el diálogo para seleccionar el archivo
        Stage stage = (Stage) cargarExcelButton.getScene().getWindow();
        File archivoCSV = fileChooser.showOpenDialog(stage);

        if (archivoCSV != null) {
            nombreArchivoLabel.setText(archivoCSV.getName());
            // Llamar al método procesarArchivoCSV para procesar el archivo CSV
            List<PCB> listaPCB = excelController.procesarArchivoCSV(archivoCSV);
            // Inicializar la tabla con los datos procesados

            TableViewInitializer.initializeTableView(tableView, FXCollections.observableList(listaPCB));
            // Realizar cualquier otra acción necesaria con los datos procesados
            List<ResultadoProcesamiento> resultados = FCFS.calc(listaPCB);
            CalculoTableView(tableViewProcesamientos, FXCollections.observableList((resultados)));

            TotalResultados totalResultados = FCFS.calcularTotales(resultados);

            TotalTableView(tableViewProcesamientosTotal, totalResultados);


        } else {
            nombreArchivoLabel.setText("Cargar CSV");
        }
    }*/

    @FXML
    private void AlgoritmoFCFS() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Guardar archivo CSV");
        // Agregar filtro para mostrar solo archivos con extensión .csv
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Archivos CSV (*.csv)", "*.csv");
        fileChooser.getExtensionFilters().add(extFilter);
        // Mostrar el diálogo para seleccionar la ubicación de guardado
        Stage stage = (Stage) cargarExcelButton.getScene().getWindow();
        File archivoCSV = fileChooser.showSaveDialog(stage);

        if (archivoCSV != null) {
            try {
                List<PCB> listaPCB = excelController.procesarArchivoCSV(archivoCSV);
                // Inicializar la tabla con los datos procesados
                TableViewInitializer.initializeTableView(tableView, FXCollections.observableList(listaPCB));
                // Realizar cualquier otra acción necesaria con los datos procesados
                List<ResultadoProcesamiento> resultados = FCFS.calc(listaPCB);
                CalculoTableView(tableViewProcesamientos, FXCollections.observableList((resultados)));
                TotalResultados totalResultados = FCFS.calcularTotales(resultados);
                TotalTableView(tableViewProcesamientosTotal, totalResultados);
                TotalPromedio promedio = new TotalPromedio();

                // Generar el archivo CSV con los resultados y la ubicación seleccionada por el usuario
                String filePath = archivoCSV.getAbsolutePath();
                Gantt gantt = new Gantt();
                gantt.crearCSV(listaPCB, resultados, promedio, filePath);

                // Actualizar las tablas para mostrar los datos procesados
                TableViewInitializer.initializeTableView(tableView, FXCollections.observableList(listaPCB));
                CalculoTableView(tableViewProcesamientos, FXCollections.observableList(resultados));
                TotalTableView(tableViewProcesamientosTotal, totalResultados);

                // Indicar al usuario que la operación se realizó correctamente
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Archivo CSV generado");
                alert.setHeaderText(null);
                alert.setContentText("El archivo CSV se ha generado correctamente en: " + filePath);
                alert.showAndWait();
            } catch (IOException e) {
                // Manejar cualquier error de E/S que pueda ocurrir durante la generación del archivo
                e.printStackTrace();
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error al generar el archivo CSV");
                alert.setHeaderText(null);
                alert.setContentText("Se produjo un error al generar el archivo CSV.");
                alert.showAndWait();
            }
        }
    }



}
