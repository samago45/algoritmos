package org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class ResultadoProcesamiento {
    private String proceso;
    private double tiempoEspera;
    private double tiempoRetorno;
    private double tiempoRespuesta;




}
