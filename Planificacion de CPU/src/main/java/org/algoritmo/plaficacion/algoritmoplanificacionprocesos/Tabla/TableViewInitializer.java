package org.algoritmo.plaficacion.algoritmoplanificacionprocesos.Tabla;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models.PCB;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models.ResultadoProcesamiento;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models.TotalResultados;

import java.util.List;

public class TableViewInitializer {
    public static void initializeTableView(TableView<PCB> tableView, ObservableList<PCB> listaPCB) {
        if (tableView.getColumns().isEmpty()) {
            // Configurar las propiedades de celda para cada columna
            TableColumn<PCB, String> procesoCol = new TableColumn<>("Proceso");
            procesoCol.setCellValueFactory(new PropertyValueFactory<>("proceso"));

            TableColumn<PCB, Integer> tiempoLlegadaCol = new TableColumn<>("T. LLegada");
            tiempoLlegadaCol.setCellValueFactory(new PropertyValueFactory<>("tiempoLlegada"));

            TableColumn<PCB, Integer> rafagasCol = new TableColumn<>("Rafagas");
            rafagasCol.setCellValueFactory(new PropertyValueFactory<>("rafagas"));

            TableColumn<PCB, Integer> prioridadCol = new TableColumn<>("Prioridad");
            prioridadCol.setCellValueFactory(new PropertyValueFactory<>("prioridad"));

            // Agregar las columnas a la tabla
            tableView.getColumns().addAll(procesoCol, tiempoLlegadaCol, rafagasCol, prioridadCol);
        }

        // Enlazar la tabla con la lista observable
        tableView.setItems(listaPCB);
    }


    public static void CalculoTableView(TableView<ResultadoProcesamiento> tableView, ObservableList<ResultadoProcesamiento> listaPCB) {
        if (tableView.getColumns().isEmpty()) {
            // Configurar las propiedades de celda para cada columna
            TableColumn<ResultadoProcesamiento, String> procesoCol = new TableColumn<>("Proceso");
            procesoCol.setCellValueFactory(new PropertyValueFactory<>("procesos"));

            TableColumn<ResultadoProcesamiento, Integer> tiempoLlegadaCol = new TableColumn<>("T. Espera");
            tiempoLlegadaCol.setCellValueFactory(new PropertyValueFactory<>("tiempoEspera"));

            TableColumn<ResultadoProcesamiento, Integer> rafagasCol = new TableColumn<>("T. Ejecucion");
            rafagasCol.setCellValueFactory(new PropertyValueFactory<>("tiempoRetorno"));

            TableColumn<ResultadoProcesamiento, Integer> prioridadCol = new TableColumn<>("T. Respuesta");
            prioridadCol.setCellValueFactory(new PropertyValueFactory<>("tiempoRespuesta"));


            TableColumn<ResultadoProcesamiento, Integer> totalEspera = new TableColumn<>("Total Espera");
            totalEspera.setCellValueFactory(new PropertyValueFactory<>("totalTiempoEspera"));

            // Agregar las columnas a la tabla
            tableView.getColumns().addAll(procesoCol, tiempoLlegadaCol, rafagasCol, prioridadCol, totalEspera);
        }

        // Enlazar la tabla con la lista observable
        tableView.setItems(listaPCB);


    }

    public static void TotalTableView(TableView<TotalResultados> tableView, TotalResultados resultados) {
        if (tableView.getColumns().isEmpty()) {
            // Configurar las propiedades de celda para cada columna
            TableColumn<TotalResultados, String> totalEspera = new TableColumn<>("Total Espera");
            totalEspera.setCellValueFactory(new PropertyValueFactory<>("totalTiempoEspera"));

            TableColumn<TotalResultados, Integer> totalEjecucion = new TableColumn<>("Total ejecucion");
            totalEjecucion.setCellValueFactory(new PropertyValueFactory<>("totalTiempoRetorno"));

            TableColumn<TotalResultados, Integer> totalRespuesta = new TableColumn<>("Total Respuesta");
            totalRespuesta.setCellValueFactory(new PropertyValueFactory<>("totalTiempoRespuesta"));

            // Agregar las columnas a la tabla
            tableView.getColumns().addAll(totalEspera, totalEjecucion, totalRespuesta);
        }

        // Crear una lista observable para los totales
        ObservableList<TotalResultados> listaTotales = FXCollections.observableArrayList();

        // Agregar un objeto TotalResultados a la lista con los valores totales
        listaTotales.add(new TotalResultados(resultados.getTotalTiempoEspera(), resultados.getTotalTiempoRetorno(), resultados.getTotalTiempoRespuesta()));

        // Enlazar la tabla con la lista de totales
        tableView.setItems(listaTotales);
    }


}

