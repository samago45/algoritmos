package org.algoritmo.plaficacion.algoritmoplanificacionprocesos.diagrama;

import com.opencsv.CSVWriter;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models.PCB;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models.ResultadoProcesamiento;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.opencsv.CSVWriter;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models.TotalPromedio;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import com.opencsv.CSVWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Gantt {
    public void crearCSV(List<PCB> pcbs, List<ResultadoProcesamiento> resultados, TotalPromedio totalPromedio, String file) throws IOException {
        // Ordenar alfabéticamente la lista de PCBs
        Comparator<PCB> comparadorPCB = Comparator.comparing(PCB::getProceso);
        pcbs.sort(comparadorPCB);

        // Calcular el tiempo total
        int tiempoTotal = pcbs.stream().mapToInt(PCB::getRafagas).sum();

        CSVWriter writer = new CSVWriter(new FileWriter(file + ".csv"));
        writer.writeNext(new String[]{file});

        // Escribir encabezado
        String[] header = new String[tiempoTotal + 1];
        header[0] = "Proceso";
        for (int i = 0; i < header.length - 1; i++) {
            header[i + 1] = Integer.toString(i);
        }
        writer.writeNext(header);

        List<String[]> allData = new ArrayList<>();

        // Escribir datos de PCBs
        for (PCB pcb : pcbs) {
            String[] data = new String[tiempoTotal + 1];
            data[0] = pcb.getProceso();

            // Calcular el momento de inicio y fin de la ejecución del proceso
            int inicioEjecucion = pcb.getTiempoLlegada(); // Empieza en el momento de llegada
            int finEjecucion = inicioEjecucion + pcb.getRafagas(); // Termina después de rafagas

            // Marcar los momentos de ejecución en el array de datos
            for (int i = inicioEjecucion + 1; i <= finEjecucion; i++) {
                data[i] = "X";
            }

            allData.add(data);
        }

        // Escribir datos adicionales (tiempos de espera, ejecución, respuesta) de ResultadoProcesamiento
        allData.add(new String[]{});
        allData.add(new String[]{"Proceso", "T. Espera", "T. Ejecucion", "T. Respuesta"});
        for (ResultadoProcesamiento resultado : resultados) {
            String[] data = new String[4];
            data[0] = resultado.getProceso();
            data[1] = Double.toString(resultado.getTiempoEspera());
            data[2] = Double.toString(resultado.getTiempoRetorno());
            data[3] = Double.toString(resultado.getTiempoRespuesta());
            allData.add(data);
        }

        // Totales y promedios
        allData.add(new String[]{"Total", Double.toString(totalPromedio.getPromedioTiempoEspera()),
                Double.toString(totalPromedio.getPromedioTiempoRetorno()),
                Double.toString(totalPromedio.getPromedioTiempoRespuesta())});
        int cantidad = pcbs.size();
        allData.add(new String[]{"Promedio", Double.toString(totalPromedio.getPromedioTiempoEspera() / cantidad),
                Double.toString(totalPromedio.getPromedioTiempoRetorno() / cantidad),
                Double.toString(totalPromedio.getPromedioTiempoRespuesta() / cantidad)});

        // Escribir en el archivo CSV
        writer.writeAll(allData);
        writer.close();
    }
}


