package org.algoritmo.plaficacion.algoritmoplanificacionprocesos;

import javafx.fxml.FXML;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.enums.Estados;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models.PCB;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models.ResultadoProcesamiento;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ExcelController {
    private static final Logger logger = LogManager.getLogger(ExcelController.class);


    public List<PCB> procesarArchivoCSV(File archivo) {
        List<PCB> listaPCB = new ArrayList<>(); // Lista para almacenar los datos leídos desde el archivo CSV

        try (BufferedReader br = new BufferedReader(new FileReader(archivo))) {
            String line;
            while ((line = br.readLine()) != null) {
                // Dividir la línea en tokens utilizando la coma como separador
                String[] tokens = line.split(",");

                // Validar la longitud mínima de tokens
                if (tokens.length < 4) {
                    logger.error("Fila no contiene suficientes datos");
                    continue;
                }

                // Convertir valores a sus tipos respectivos
                String proceso = tokens[0];
                int tiempoLlegada = Integer.parseInt(tokens[1]);
                int rafagas = Integer.parseInt(tokens[2]);
                int prioridad = Integer.parseInt(tokens[3]);

                // Agregar los datos a la lista de PCB
                listaPCB.add(new PCB(proceso, tiempoLlegada, rafagas, prioridad, Estados.EJECUCION));
            }
        } catch (IOException e) {
            logger.error("Error al procesar el archivo CSV: " + e.getMessage(), e);
        } catch (NumberFormatException e) {
            logger.error("Error al convertir a número: " + e.getMessage(), e);
        }

        return listaPCB;
    }


}
