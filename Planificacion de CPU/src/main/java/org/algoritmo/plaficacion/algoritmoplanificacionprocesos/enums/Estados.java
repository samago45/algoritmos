package org.algoritmo.plaficacion.algoritmoplanificacionprocesos.enums;

public enum Estados {
    LISTO,
    EJECUCION,
    ESPERA,
    TERMINADO
}