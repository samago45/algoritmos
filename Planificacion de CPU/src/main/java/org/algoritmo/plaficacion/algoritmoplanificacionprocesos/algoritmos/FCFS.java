package org.algoritmo.plaficacion.algoritmoplanificacionprocesos.algoritmos;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models.PCB;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models.ResultadoProcesamiento;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models.TotalPromedio;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models.TotalResultados;

import java.util.*;

public class FCFS {

    public static List<ResultadoProcesamiento> calc(List<PCB> procesos) {
        List<ResultadoProcesamiento> resultados = new ArrayList<>();
        List<PCB> procesosCopiados = new ArrayList<>(procesos);
        ordenarPorTiempoLlegada(procesosCopiados);

        int tiempoActual = 0;
        double totalTiempoEspera = 0;
        double totalTiempoRetorno = 0;
        double totalTiempoRespuesta = 0;

        for (PCB proceso : procesosCopiados) {
            if (tiempoActual < proceso.getTiempoLlegada()) {
                tiempoActual = proceso.getTiempoLlegada();
            }
            proceso.getInicio_ejecucion().add(tiempoActual);
            tiempoActual += proceso.getRafagas();
            proceso.getFin_ejecucion().add(tiempoActual);

            int tiempoEspera = proceso.getInicio_ejecucion().get(0) - proceso.getTiempoLlegada();
            int tiempoRetorno = proceso.getRafagas() + tiempoEspera;
            int tiempoRespuesta = tiempoRetorno - proceso.getTiempoLlegada();

            // Actualizar totales sin sumar dentro del bucle
            /*totalTiempoEspera += tiempoEspera;
            totalTiempoRetorno += tiempoRetorno;
            totalTiempoRespuesta += tiempoRespuesta;
*/
            ResultadoProcesamiento resultadoProcesamiento = new ResultadoProcesamiento(
                    proceso.getProceso(),
                    tiempoEspera,
                    tiempoRetorno,
                    tiempoRespuesta

            );
            resultados.add(resultadoProcesamiento);
        }

        return resultados;
    }

    public static TotalResultados calcularTotales(List<ResultadoProcesamiento> resultadosProcesamiento) {
        double totalTiempoEspera = 0;
        double totalTiempoRetorno = 0;
        double totalTiempoRespuesta = 0;

        for (ResultadoProcesamiento resultado : resultadosProcesamiento) {
            totalTiempoEspera += resultado.getTiempoEspera(); // Suma directamente el tiempoEspera
            totalTiempoRetorno += resultado.getTiempoRetorno(); // Suma directamente el tiempoRetorno
            totalTiempoRespuesta += resultado.getTiempoRespuesta(); // Suma directamente el tiempoRespuesta
        }

        return new TotalResultados(totalTiempoEspera, totalTiempoRetorno, totalTiempoRespuesta);
    }






    //Metodos Privados
    // Método para ordenar los procesos por tiempo de llegada
    private static void ordenarPorTiempoLlegada(List<PCB> procesos) {
        Collections.sort(procesos, Comparator.comparingInt(PCB::getTiempoLlegada));
    }

   /* public static void mostrarResultadosEnTabla(ResultadoProcesamiento resultado, TableView tablaProcesos) {
        List<PCB> procesos = resultado.getProcesos();

        // Limpiar la tabla antes de agregar nuevos datos
        tablaProcesos.getItems().clear();

        // Agregar procesos a la tabla
        for (PCB proceso : procesos) {
            ObservableList<String> filaProceso = FXCollections.observableArrayList();
            filaProceso.add(proceso.getProceso());
            for (int i = 0; i < 25; i++) {
                if (i >= proceso.getInicio_ejecucion().get(0) && i <= proceso.getFin_ejecucion().get(0)) {
                    filaProceso.add("X");
                } else {
                    filaProceso.add("");
                }
            }
            tablaProcesos.getItems().add(filaProceso);
        }
    }*/
}