package org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.algoritmo.plaficacion.algoritmoplanificacionprocesos.enums.Estados;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter

public class PCB {
    private String proceso;
    private int tiempoLlegada;
    private int rafagas;
    private int prioridad;
    private Estados estado;


    private List<Integer> inicio_ejecucion = new ArrayList<Integer>();
    private List<Integer> fin_ejecucion = new ArrayList<Integer>();


    public PCB(String proceso, int tiempoLlegada, int rafagas, int prioridad , Estados estado) {
        this.proceso = proceso;
        this.tiempoLlegada = tiempoLlegada;
        this.rafagas = rafagas;
        this.prioridad = prioridad;
        this.estado = estado;
    }





}
