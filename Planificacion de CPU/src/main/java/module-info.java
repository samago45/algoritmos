module org.algoritmo.plaficacion.algoritmoplanificacionprocesos {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.apache.logging.log4j;
    requires static lombok;
    requires com.opencsv;


    opens org.algoritmo.plaficacion.algoritmoplanificacionprocesos to javafx.fxml;
    exports org.algoritmo.plaficacion.algoritmoplanificacionprocesos;
    exports org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models;
    opens org.algoritmo.plaficacion.algoritmoplanificacionprocesos.models to javafx.fxml;
    exports org.algoritmo.plaficacion.algoritmoplanificacionprocesos.Tabla;
    opens org.algoritmo.plaficacion.algoritmoplanificacionprocesos.Tabla to javafx.fxml;


}